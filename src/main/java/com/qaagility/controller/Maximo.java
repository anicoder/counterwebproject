package com.qaagility.controller;

public class Maximo {

    public int findMax(int paramA, int paramB) {
        if (paramB == 0)
            return Integer.MAX_VALUE;
        else
            return paramA / paramB;
    }

}
